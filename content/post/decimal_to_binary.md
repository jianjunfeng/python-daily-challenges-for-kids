---
title: "Decimal_to_binary"
subtitle: "Convert a base10 number to a base 2 number"
tags: ["intermediate","string", "number", "loop"]
date: 2021-08-15T12:02:30-05:00
draft: false
---


### Write a function to convert a base 10 integer, i.e., decimal integer, to a base 2, i.e. binary, number in string format 

The function skeleton looks like this:
Given a number n ,
return the binary number in string format 
For example, n=5, output: '101'

```python
def decimal_to_binary(n):
    pass
```


## Test your function

```python
n=5
b= decimal_to_binary(n)
print(b)

```




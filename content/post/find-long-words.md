---
title: "Find the long words in a text"
subtitle: "Learn to use string, list, set, and loop "
tags: ["intermediate","list", "set", "loop"]
date: 2021-06-11
draft: false
---


### Write a function to find the unique words in the given text that are at least n-letter long

The function skeleton looks like this:
Given a long string of English text and number n (n>0),
return the unique words that are longer than n


```python
def find_long_words(text, n):
    pass
```


## Test your function

```python
text='''
Either the well was very deep, or she fell very slowly, for she had plenty of time as she went down to look about her and to wonder what was going to happen next. First, she tried to look down and make out what she was coming to, but it was too dark to see anything; then she looked at the sides of the well, and noticed that they were filled with cupboards and book-shelves; here and there she saw maps and pictures hung upon pegs. She took down a jar from one of the shelves as she passed; it was labelled `ORANGE MARMALADE', but to her great disappointment it was empty: she did not like to drop the jar for fear of killing somebody, so managed to put it into one of the cupboards as she fell past it.
'''

unique_words = find_long_words(text, 5)
print(unique_words)

```

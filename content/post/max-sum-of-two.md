---
title: "Find the max value of the sum of two numbers out of three"
subtitle: "Learn to use if-else, comparison"
tags: ["beginner","if", "comparison", "function"]
date: 2021-06-27
draft: false
---


### Find the max value of the sum of two numbers out of three"


The function skeleton looks like this:


Given three numbers 
return the max possible value of the sum of any two numbers 


```python
def find_max_sum_of_two(a,b,c):
    pass
```


## Test your function

```python
print(find_max_sum_of_two(2,1,3)) #max is 5
print(find_max_sum_of_two(3,2,3)) #max is 6
print(find_max_sum_of_two(3,3,3)) #max is 6


```

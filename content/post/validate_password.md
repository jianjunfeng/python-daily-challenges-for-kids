---
title: "Check the complexity of a password"
subtitle: "Learn to use loop, if-else and boolean variables"
tags: ["beginner","for-loop","boolean"]
date: 2021-07-18
draft: false
---

### Write a function to verify the password complexity

A company sets the following rules for its password complexity.


1. The length of a password is at least 8
2. The length of a password is at most 32
3. A password must contain at least one upper case letter
4. A password must contain at least one lower case letter
5. A password must contain at least one number

Write a function to return True if a given password meets ALL the 5 rules, otherwise, return False.

```python
def is_valid(password):
    pass #Write your code here

##test your function
c = is_valid("badPass")
print(c) #should be False

c = is_valid("G00dP4sS")
print(c) #should be True

```

<div class="panel panel-info">
**Note**
{: .panel-heading}
<div class="panel-body">

HINT:

KC,

You may find the following methods useful:

```python
c = "A"
c.isupper() #return True
c.islower() #return False
c.isdigit() #return False
```


</div>
</div>


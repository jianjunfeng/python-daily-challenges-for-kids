---
title: "Convert year to cnetury"
subtitle: "Learn to use operator % and // "
tags: ["beginner","% mod operator"]
date: 2021-06-06
draft: false
---

### Write a function to convert year to century

The function skeleton looks like this:

```python
def year_to_century(year):
    pass

##test your function
c = year_to_century(2021)
print(c)

```
